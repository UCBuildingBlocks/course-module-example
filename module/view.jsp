<%@ taglib uri="/bbNG" prefix="bbNG"%>
<bbNG:includedPage>
<%
// if inside a course
if(bbContext.hasCourseContext()){ 
%>
You are in course <%=bbContext.getCourse().getCourseId() %>.
<%
//if not logged in and module exists on a tab
}else if (bbContext.getUser().getUserName().equalsIgnoreCase("guest")){ 
%>
You are not logged in.
<%
}else{ 
//if logged in and module exists on a tab
%>
You are logged in, <%=bbContext.getUser().getUserName() %>.
<%}%>
</bbNG:includedPage>